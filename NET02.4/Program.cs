﻿using System;
using System.Threading;
using SiteConfig;

namespace NET02._4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (new Mutex(false, "6238EF04 - E2C9 - 4B4C - B7C4 - EA5DA587F4D5", out bool ex))
            {
                if (!ex)
                {
                    Console.WriteLine("App has been already run, press any key to close");
                    Console.ReadLine();
                    return;
                }

                Monitoring monitoring = new Monitoring
                {
                    SiteSection = "SiteSection",
                    LogSection = "logger",
                    SenderMessages = new Sender
                    {
                        EmailSender = "senderfromapp@gmail.com",
                        PasswordSender = "12345678_k"
                    }
                };
                monitoring.Start();

                Console.WriteLine("App is running, press any key to close");
                Console.ReadLine();
            }
        }
    }
}