﻿using System;
using System.Net.NetworkInformation;
using LoggerDll;

namespace SiteConfig
{
    public class Site
    {
        private static readonly object Locker = new object();
        private string _email;
        private int _expectation;
        private int _interval;
        private string _name;

        public int Expectation
        {
            get => _expectation;
            set
            {
                CheckArgs(value);
                _expectation = value;
            }
        }

        public int Interval
        {
            get => _interval;
            set
            {
                CheckArgs(value);
                _interval = value;
            }
        }

        public string Name
        {
            get => _name;
            set
            {
                CheckArgs(value);
                _name = value;
            }
        }

        public string Email
        {
            get => _email;
            set
            {
                CheckArgs(value);
                _email = value;
            }
        }

        public void TestSite(Sender sender)
        {
            CheckArgs(sender);
            long siteAvailable;
            try
            {
                using (var ping = new Ping())
                {
                    siteAvailable = long.Parse(ping.Send(Name)?.RoundtripTime.ToString() ??
                                               throw new InvalidOperationException());
                }
            }
            catch (PingException)
            {
                SendingMessage.SendMessageAsync(this, "due to site doesn't exist", sender.EmailSender, Email,
                    sender.PasswordSender);
                return;
            }

            if (siteAvailable - Expectation > 0)
            {
                SendingMessage.SendMessageAsync(this, "due to response time is exceeded", sender.EmailSender, Email,
                    sender.PasswordSender);
                return;
            }

            Logger logger = new Logger();
            lock (Locker)
            {
                logger.Info(Name + " is available");
            }
        }

        private void CheckArgs(object arg)
        {
            if (arg is null)
                throw new ArgumentNullException(nameof(arg));
        }
    }
}